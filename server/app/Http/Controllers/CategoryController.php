<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;

class CategoryController extends Controller
{
    public function index()
    {
        
        $categories = Category::all();
        return response()->json([
            'response' => 200,
            'success' => true,
            'data' => $categories
            ]);
    }
 
    public function show($id)
    {
        $category = Category::find($id);
        return response()->json([
            'response' => 200,
            'success' => true,
            'data' => $category
            ]);
    }

    public function store(Request $request)
    {
        $category = new Category;
        $category->name = $request->name;
        $category->save();

        return response()->json([
            'response' => 200,
            'success' => true,
            'data' => $category
            ]);

    }

    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->name = $request->name;
        $category->save();

        return response()->json([
            'response' => 200,
            'success' => true,
            'data' => $category
            ]);
    }

    public function delete(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        $category->posts()->sync([]);
        return response()->json([
            'response' => 200,
            'success' => true
            ]);
    }}
