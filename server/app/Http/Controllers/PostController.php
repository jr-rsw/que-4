<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;

class PostController extends Controller
{
    
    public function index()
    {
        $post = Post::with('categories')->get();
        return response()->json([
            'response' => 200,
            'success' => true,
            'data' => $post
            ]);
    }
 
    public function show($id)
    {
        return Post::with('categories')->find($id);
    }

    public function store(Request $request)
    {
        $post = new Post;
        $post->title = $request->title;
        $post->body = $request->body;
        $post->save();

        $category = Category::find($request->categoryIds);
        $post->categories()->attach($category);
        return response()->json([
            'response' => 200,
            'success' => true,
            'data' => $post
            ]);

    }

    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->save();

        $post->categories()->sync([]);
        $category = Category::find($request->categoryIds);
        $post->categories()->attach($category);
        return response()->json([
            'response' => 200,
            'success' => true,
            'data' => $post
            ]);
    }

    public function delete(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        $post->categories()->sync([]);
        return response()->json([
            'response' => 200,
            'success' => true
            ]);
    }
}
