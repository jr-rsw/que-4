import axios from 'axios'

const client = axios.create({
  baseURL: 'http://localhost:8000/api',
  json: true
})

export default {
  async execute (method, resource, data) {
    return client({
      method,
      url: resource,
      data,
      headers: {
        'Content-Type': `application/json`
      }
    }).then(req => {
      return req.data
    })
  },
  getPosts () {
    return this.execute('get', '/posts')
  },
  getPost (id) {
    return this.execute('get', `/posts/${id}`)
  },
  createPost (data) {
    return this.execute('post', '/posts', data)
  },
  updatePost (id, data) {
    return this.execute('put', `/posts/${id}`, data)
  },
  deletePost (id) {
    return this.execute('delete', `/posts/${id}`)
  },
  getCategories () {
    return this.execute('get', '/categories')
  },
  createCategory (data) {
    return this.execute('post', '/categories', data)
  },
  updateCategory (id, data) {
    return this.execute('put', `/categories/${id}`, data)
  },
  deleteCategory (id) {
    return this.execute('delete', `/categories/${id}`)
  }
}
