import Vue from 'vue'
import Router from 'vue-router'
//
import Hello from '@/components/Hello'
import Posts from '@/components/Posts'
import Categories from '@/components/Categories'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/posts',
      name: 'Posts',
      component: Posts
    },
    {
      path: '/categories',
      name: 'Categories',
      component: Categories
    }
  ]
})

export default router
