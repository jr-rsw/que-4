<h1 align="center">Vue CRUD</h1>

## SERVER
### Framework used

- Laravel 7.X

### Server Requirements

- PHP >= 7.2.5
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

### License

GNU General Public License v3.0

### Features

This software has following features:

| Feature | Description |
|---------|-------------|
| Categories  | News, Sport, Travel. |
| Posts | A post belongs to multiple categories. |

### How to Start
Here are some basic steps to start using this application

- Clone the repository

```sh
git clone https://jr-rsw@bitbucket.org/jr-rsw/que-4.git
cd server 
```

- Copy the contents of the `.env.example` file to create `.env` in the same directory

- Run `composer install` for `developer` environment and 

- Generate `APP_KEY` using `php artisan key:generate`

- Edit the database connection configuration in .env file e.g.

```sh
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE={{database_name}}
DB_USERNAME={{database_user_name}}
DB_PASSWORD={{secret}}
```

> Note that this is just an example, and the values may vary depending on your database environment.

- Migrate your Database with `php artisan migrate`

- On localhost, serve your application with `php artisan serve`

- serve with hot reload at localhost:8000 

## web-vue-app

### Build Setup

``` bash
# install dependencies
cd vue-crud-app
npm install

# Listening at http://localhost:8080
npm run dev
```

### Basic Steps by Serial

* Create Categories
* Create Posts

### Demo Video
> See **[Video Tutorial](https://www.loom.com/share/d3992cf360164ea787fda14cded28043)**.
## Here are some screenshots:
![Screenshot_2020-06-24_12-55-42](https://user-images.githubusercontent.com/21338587/85512344-9b0e4f80-b61b-11ea-804d-d727d4b6868c.png)
![Screenshot_2020-06-24_12-56-38](https://user-images.githubusercontent.com/21338587/85512341-9a75b900-b61b-11ea-9fc6-3510d39d740f.png)
![Screenshot_2020-06-24_12-59-35](https://user-images.githubusercontent.com/21338587/85512336-99dd2280-b61b-11ea-91be-2c1fc4d0fff4.png)
![Screenshot_2020-06-24_13-05-05](https://user-images.githubusercontent.com/21338587/85512329-98135f00-b61b-11ea-9bd0-7278484189ed.png)

